<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentInterest extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'student_student_interest';

}