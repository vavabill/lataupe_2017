<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'students';


    /**
     * Get the associated themes
     * @return belongsToMany The relationship
     */
    public function themes()
    {
        return $this->belongsToMany(StudentInterest::class);
    }

    /**
     * Get a list of themes ids associated with the current student
     *
     * @return array
     */
    public function getThemeListAttribute()
    {
        return $this->themes->pluck('id')->all();
    }

}