<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class StatutJeune extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'statut_jeunes';

}
