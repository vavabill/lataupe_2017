<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'levels';

}
