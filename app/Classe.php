<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Classe extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'class';

}
