<?php namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{

    /**
     * Instanciate a new Request instance
     */
    public function __construct()
    {
        $this->validator = app('validator');

        $this->validatePhoneNumber($this->validator);
    }

    /**
     * Validate the input.
     *
     * @param  \Illuminate\Validation\Factory  $factory
     * @return \Illuminate\Validation\Validator
     */
    public function validator($factory)
    {
        return $factory->make(
            $this->sanitizeInput(), $this->container->call([$this, 'rules']), $this->messages()
        );
    }

    /**
     * Sanitize the input.
     *
     * @return array
     */
    protected function sanitizeInput()
    {
        if (method_exists($this, 'sanitize')) {
            return $this->container->call([$this, 'sanitize']);
        }

        return $this->all();
    }


    /**
     * Extend validator to check for correct phone or fax french format
     * @param  Validator $validator
     * @return boolean
     */
    public function validatePhoneNumber($validator)
    {
        $validator->extend('phone', function($attribute, $value)
        {
            return preg_match('/^0[1-9][0-9]{8}$/', strval($value));
        });
    }
}
