<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SaveContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'string|max:255',
            'firstname'         => 'string|max:255',
            'establishment'     => 'string|max:255',
            'email'             => 'required|email',
            'phone'             => 'phone',
            'subject'           => 'required|string|max:255',
            'message'           => 'required|max:30000',
        ];
    }
}
