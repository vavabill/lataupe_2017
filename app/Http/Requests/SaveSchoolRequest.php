<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
//use Illuminate\Support\Facades\Input;

class SaveSchoolRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules
     * @var array
     */
    public function rules()
    {
        return [
            'establishment' => 'required',
            'cp'            => 'required',
            'city'          => 'required',
            'email'         => 'required|email',
            'phone'         => 'phone',

            'fonction'      => 'string|max:255',
            'name'          => 'string|max:255',
            'firstname'     => 'string|max:255',
            'contact_email' => 'required|email',
        ];
    }
}
