<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
//use Illuminate\Support\Facades\Input;

class SaveStudentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Validation rules
     * @var array
     */
    public function rules () {

        $rules = $this->rules;

        $rules['statut_jeunes'] = 'required';

        if ($this->request->get('statut_jeunes') == 2 )
        {
            $rules['class']     = 'required';
            $rules['bacs']      = 'required';
        }
        elseif ($this->request->get('statut_jeunes') == 3 )
        {
            $rules['levels']    = 'required';
            $rules['themes']    = '';
        }

        $rules['civility']      = 'required';
        $rules['name']          = 'required';
        $rules['firstname']     = 'required';
        $rules['birthdate']     = 'required|date_format:d/m/Y|regex:/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/';
        $rules['phone']         = '';
        $rules['email']         = 'required|email';
        $rules['cp']            = 'required';
        $rules['city']          = 'required';
        //$rules['themes_list']   = 'required|array';

        return $rules; 
       
    }
}
