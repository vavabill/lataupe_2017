<?php namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Student;
use App\StudentInterest;
use App\StatutJeune;
use App\Classe;
use App\Bac;
use App\Level;
use App\Theme;

use App\Http\Requests\SaveStudentRequest;
use Illuminate\Support\Facades\Input;

use Mail;
use Session;


class StudentsController extends Controller
{
    
    /**
    *   CONFIRMATION
    */
    public function confirmation()
    {
        return view('confirmations/student');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $statut_jeunes  = StatutJeune::orderBy('id', 'ASC')->pluck('name', 'id')->all();
        $class          = Classe::orderBy('id', 'ASC')->pluck('name', 'id')->all();
        $bacs           = Bac::orderBy('id', 'ASC')->pluck('name', 'id')->all();
        $levels         = Level::orderBy('id', 'ASC')->pluck('name', 'id')->all();
        $themes         = Theme::all()->pluck('name', 'id')->all();

        return view('student', compact('statut_jeunes', 'class', 'bacs', 'levels', 'themes'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SaveStudentRequest $request)
    {
            $student       = new Student();

            $this->saveStudent($request, $student);

            $statut_jeunes  = StatutJeune::all();
            $classes        = Classe::all();
            $bacs           = Bac::all();
            $levels         = Level::all();
            $themes         = Theme::all();
            $interests      = StudentInterest::all();

            // Send the email with the contactemail view, the user input
            Mail::send('emails/student-email', compact('student', 'statut_jeunes', 'classes', 'bacs', 'levels', 'themes', 'interests'), function($message) use ($student, $request)
            {
                $message->to('info@lataupe.fr')->subject('Nouvelle inscription LATAUPE');
            });

            // Redirect with confirmation message
            //Session::flash('message', 'Votre inscription a bien été transmise.');
            //return redirect('student');
            return view('confirmations/student', compact('student'));
    }


    /**
     * Save student to DB
     * @param  Request $request
     * @param  Student $student
     * @return Student
     */
    public function saveStudent(SaveStudentRequest $request, Student $student)
    {
        
        $student->statut_id        = $request->statut_jeunes;
        $student->class_id         = $request->class;
        $student->bac_id           = $request->bacs;
        $student->level_id         = $request->levels;
        $student->theme_id         = $request->themes;

        $student->civility         = $request->civility;
        $student->name             = $request->name;
        $student->firstname        = $request->firstname;
        $student->birthdate        = $request->birthdate;
        $student->phone            = $request->phone;
        $student->email            = $request->email; 
        $student->cp               = $request->cp;
        $student->city             = $request->city;

        $student->save();

        $this->syncStudentInterests($student, $request->input('interest_list'));

        return $student;
    }

    /**
     * Sync themes
     * @param  student $student
     * @param  array|null $themes
     * @return void
     */
    private function syncStudentInterests( Student $student, $themes)
    {
        if ($themes == null) {
            $themes = [];
        }

        $student->themes()->sync($themes);
    }

}
