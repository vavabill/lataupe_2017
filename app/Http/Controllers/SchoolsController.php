<?php namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\School;

use App\Http\Requests\SaveSchoolRequest;

use Mail;
use Session;


class SchoolsController extends Controller
{
    
    /**
    *   CONFIRMATION
    */
    public function confirmation()
    {
        return view('confirmations/ecole');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        return view('ecole');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SaveSchoolRequest $request)
    {
            $school       = new School();

            $this->saveSchool($request, $school);

            // Send the email with the contactemail view, the user input
            Mail::send('emails/school-email', compact('school'), function($message) use ($school, $request)
            {
                $message->to('info@lataupe.fr')->subject('Nouvelle inscription LATAUPE');
            });

            // Redirect with confirmation message
            //Session::flash('message', 'Votre inscription a bien été transmise.');
            //return redirect('ecole');
            return view('confirmations/ecole', compact('school'));
    }


    /**
     * Save school to DB
     * @param  Request $request
     * @param  school $school
     * @return school
     */
    public function saveSchool(SaveSchoolRequest $request, School $school)
    {

        $school->establishment      = $request->establishment;
        $school->cp                 = $request->cp;
        $school->city               = $request->city;
        $school->email              = $request->email;
        $school->phone              = $request->phone;

        $school->fonction           = $request->fonction;
        $school->name               = $request->name;
        $school->firstname          = $request->firstname;
        $school->contact_email      = $request->contact_email;

        $school->save();

        return $school;
    }

}
