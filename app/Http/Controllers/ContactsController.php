<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;

use App\Http\Requests\SaveContactRequest;

use Session;
use Illuminate\Support\Facades\Input;

class ContactsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('contact');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveContactRequest $request)
    {

        $contact = new Contact();

        $this->saveContact($request, $contact);

        // Send the email with the contactemail view, the user input
        Mail::send('emails/contact-email', compact('contact'), function($message) use ($contact, $request)
        {
            $message->from($contact->email, 'Nouveau contact La Taupe');

            $message->to('info@lataupe.fr')->subject('Nouveau contact La Taupe');
        });

        // Redirect with confirmation message
        Session::flash('message', 'Votre message a bien été transmis.');
        return redirect('/contact');       
    }

    /**
     * Save contact to DB
     * @param  Request $request
     * @param  Contact $contact
     * @return Contact
     */
    public function saveContact(SaveContactRequest $request, Contact $contact)
    {
        
        $contact->name      	= $request->name;
        $contact->firstname     = $request->firstname;
        $contact->establishment = $request->establishment;
        $contact->email         = $request->email;
        $contact->phone         = $request->phone;

        if ($request->subject != '') {
           $contact->subject       = $request->subject;
        }
        if ($request->message != '') {
            $contact->message       = $request->message;
        }
        

        $contact->save();

        return $contact;
    }

}
