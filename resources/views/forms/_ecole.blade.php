{!! Form::open([
    'action' => 'SchoolsController@store'
]) !!}

	<!-- NAME -->
	<label class="col-md-3">Nom de l'établissement *</label> 
	<div class="col-md-9">
		{!! Form::text('establishment', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('establishment') }}</small>
	</div>
	<div class="clear"></div>

	<!-- ADDRESS -->
	<label class="col-md-3">Code postal *</label> 
	<div class="col-md-9">
		{!! Form::text('cp', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('cp') }}</small>
	</div>
	<div class="clear"></div>

	<label class="col-md-3">Ville *</label>
	<div class="col-md-9">
		{!! Form::text('city', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('city') }}</small>
	</div>
	<div class="clear"></div>

	<!-- MAIL -->
	<label  class="col-md-3">Email général *</label>
	<div class="col-md-9">
		{!! Form::text('email', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('email') }}</small>
	</div>
	<div class="clear"></div>

	<!-- PHONE -->
	<label class="col-md-3">Téléphone (standard) *</label>
	<div class="col-md-9">
		{!! Form::text('phone', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('phone') }}</small>
	</div>
	<div class="clear"></div>

	<br/><hr/>
	<h4>Interlocuteur</h4>

	<!-- FUNCTION -->
	<label class="col-md-3">Fonction *</label> 
	<div class="col-md-9">
		{!! Form::text('fonction', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('fonction') }}</small>
	</div>
	<div class="clear"></div>

	<!-- NAME -->
	<label class="col-md-3">Nom *</label> 
	<div class="col-md-9">
		{!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('name') }}</small>
	</div>
	<div class="clear"></div>

	<!-- FIRSTNAME -->
	<label class="col-md-3">Prénom *</label> 
	<div class="col-md-9">
		{!! Form::text('firstname', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('firstname') }}</small>
	</div>
	<div class="clear"></div>

	<!-- MAIL -->
	<label  class="col-md-3">Email *</label>
	<div class="col-md-9">
		{!! Form::text('contact_email', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('contact_email') }}</small>
	</div>
	<div class="clear"></div>


	<div class="col-md-12">
		<br/>
		<p>* Champs obligatoires</p>
	</div>

	<div class="clear"></div>

	<div class="col-sm-12">
	    {!! Form::submit('S\'inscrire', ['class' => 'btn btn-primary btn-send']) !!}
	</div>

	<div class="clear"></div>

{!! Form::close() !!}	