{!! Form::open([
    'action' => 'StudentsController@store'
]) !!}

	<label class="col-md-3 col-xs-12">Je suis *</label>
	{!! Form::select('statut_jeunes', $statut_jeunes, null, ['id' => 'statut_jeunes', 'class' => 'statut col-md-9 col-xs-12', 'placeholder' => 'Sélectionne ton statut', 'required']) !!}
	<small class="text-danger">{{ $errors->first('statut_jeunes') }}</small>

	<!-- IF LYCEEN -->
	<div class="lyceen box">
		<label class="col-md-3 col-xs-12">Classe *</label>
		{!! Form::select('class', $class, null, ['id' => 'class', 'class' => 'col-md-9 col-xs-12', 'placeholder' => 'Sélectionne ta classe']) !!}
		<small class="text-danger">{{ $errors->first('class') }}</small>

		<label class="col-md-3 col-xs-12">Bac *</label>
		{!! Form::select('bacs', $bacs, null, ['id' => 'bacs', 'class' => 'col-md-9 col-xs-12', 'placeholder' => 'Sélectionne le bac que tu prépares']) !!}
		<small class="text-danger">{{ $errors->first('bac') }}</small>
	</div>

	<!-- IF ETUDIANT -->
	<div class="etudiant box">
		<label class="col-md-3 col-xs-12">Niveau *</label>
		{!! Form::select('levels', $levels, null, ['id' => 'levels', 'class' => 'col-md-9 col-xs-12', 'placeholder' => 'Sélectionne ton niveau']) !!}
		<small class="text-danger">{{ $errors->first('levels') }}</small>

		<label class="col-md-3 col-xs-12">Domaine</label>
		{!! Form::select('themes', $themes, null, ['id' => 'themes', 'class' => 'col-md-9 col-xs-12', 'placeholder' => 'Sélectionne le domaine que tu étudies']) !!}
		<small class="text-danger">{{ $errors->first('themes') }}</small>
	</div>

	<div class="clear"></div>
	<!-- CIVILITY -->
	<label class="col-md-3">Civilité</label> 
	<div class="col-md-9">
		<label class="civility"><input type="radio" name="civility" value="1">Homme</label>
		<label class="civility"><input type="radio" name="civility" value="2">Femme</label>
	</div>
	<small class="text-danger">{{ $errors->first('civility') }}</small>
	<div class="clear"></div>

	<!-- NAME -->
	<label class="col-md-3">Nom *</label> 
	<div class="col-md-9">
		{!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('name') }}</small>
	</div>

	<!-- FIRSTNAME -->
	<label class="col-md-3">Prénom *</label> 
	<div class="col-md-9">
		{!! Form::text('firstname', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('firstname') }}</small>
	</div>

	<!-- BIRTH -->
	<label class="col-md-3">Naissance *</label>
	<div class="col-md-9">
		{!! Form::text('birthdate', null, ['class' => 'form-control', 'placeholder' => ' ex : 12/05/2003', 'required']) !!}
		<small class="text-danger">{{ $errors->first('birthdate') }}</small>
	</div>

	<!-- PHONE -->
	<label class="col-md-3">Téléphone</label>
	<div class="col-md-9">
		{!! Form::text('phone', null, ['class' => 'form-control']) !!}
		<small class="text-danger">{{ $errors->first('phone') }}</small>
	</div>

	<!-- MAIL -->
	<label  class="col-md-3">Adresse mail *</label>
	<div class="col-md-9">
		{!! Form::text('email', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('email') }}</small>
	</div>

	<!-- ADDRESS -->
	<label class="col-md-3">Code postal *</label> 
	<div class="col-md-9">
		{!! Form::text('cp', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('cp') }}</small>
	</div>

	<label class="col-md-3">Ville *</label>
	<div class="col-md-9">
		{!! Form::text('city', null, ['class' => 'form-control', 'required']) !!}
		<small class="text-danger">{{ $errors->first('city') }}</small>
	</div>


	<label class="col-md-3 col-xs-12">Domaine(s) qui m'intéresse(nt) *</label>
	<div class="col-md-9">
		{!! Form::select('interest_list[]', $themes, null, ['id' => 'interest_list', 'class' => 'form-control', 'required' => 'required', 'multiple']) !!}
	</div>

	<div class="col-md-12">
		<br/>
		<p>* Champs obligatoires</p>
	</div>

	<div class="clear"></div>

	<div class="col-sm-12">
	    {!! Form::submit('Je m\'inscris', ['class' => 'btn btn-primary btn-send']) !!}
	</div>

	<div class="clear"></div>

{!! Form::close() !!}	