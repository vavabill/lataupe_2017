<div class="col-sm-6">
    <div class="form-group">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nom *', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder' => 'Prénom *', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('firstname') }}</small>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::text('establishment', null, ['class' => 'form-control', 'placeholder' => 'Établissement']) !!}
        <small class="text-danger">{{ $errors->first('establishment') }}</small>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email *', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('email') }}</small>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Téléphone']) !!}
        <small class="text-danger">{{ $errors->first('phone') }}</small>
    </div>
</div>
<div class="clear"></div>

<div class="col-sm-12">
    <div class="form-group">
        <br/>
        {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Type de demande *', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('subject') }}</small>
    </div>
</div>
<div class="col-sm-12">
    <div class="form-group">
        {!! Form::textarea('message', null, ['class' => 'form-control', 'size' => '30x5', 'placeholder' => 'Message *', 'required' => 'required' ]) !!}
        <small class="text-danger">{{ $errors->first('message') }}</small>
    </div>
</div>