@extends('layouts.app')

@section('title')
    LA TAUPE confirmation inscription étudiant
@stop

@section('content')

    <div class="full center confirmation student">
        <nav id="nav" class="navbar navbar-default navbar-inverse navbar-fixed-top">

            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                <i class="fa fa-bars"></i>
              </button>
              <a href="/">
                  <div class="logo-container">
                      <div class="logo">
                          <img src="/images/logo.png" alt="Hello Extra" title="Hello Extra - accueil" />
                      </div>
                  </div>
              </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu" >
                <ul class="nav navbar-nav navbar-right">
                    <li> 
                        <a href="/student" alt="LA TAUPE - étudiant">ACCUEIL ÉTUDIANTS</a>
                    </li>
               </ul>
            </div><!-- /.navbar-collapse -->

        </nav>

        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="jumbotron">
                <h1>MERCI !!!</h1>

                <h2>Bienvenue dans la communauté la taupe !</h2>

                <p>Adresse email d'inscription : {{ $student->email }}</p>

                <div class="content">
                    <p>
                        Sois le 1<sup>er</sup> à utiliser l'appli La Taupe !
                    </p>
                    <p>
                        On se tient au courant très vite du lancement !
                    </p>
                </div>
                <div class="line"></div>
                <div class="content">
                    <p>En attendant, rejoins-nous :</p>
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/lataupe.orientation/?fref=ts" alt="Facebook La Taupe" title="Facebook La Taupe" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#" alt="Instagram La Taupe" title="Instagram La Taupe" target="_blank">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="clear"></div>         
        </div>
        
        <div class="clear"></div>
    </div>

@endsection

