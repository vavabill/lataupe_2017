<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="_5wbw9VPYiX-iDEm1HyxKpUYo4dbLgmhpL0pV-uQb-U" />

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title')</title>

        <meta name="description" content="@yield('description')">

        <!-- Styles -->
        <link href="/css/app.css" rel="stylesheet">
    </head>
    <body>

        @include('layouts.analyticstracking')

        @if (Session::has('message') || Session::has('alert'))
            <div class="container alert-container">
                <div class="col-md-9 col-md-offset-1">
                    {{-- Will be used to show messages --}}
                    @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ Session::get('message') }}
                        </div>
                    @elseif (Session::has('danger'))
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ Session::get('danger') }}
                        </div>
                    @endif
                </div>
            </div>
        @endif

        <div class="content" >

            @yield('content')

        </div>
     
        <!-- Scripts -->  
        <script src="/js/app.js"></script>
        <script src="/js/front.js"></script>

        @if(Session::has('errors'))
            <script type="text/javascript">
                $('#etudiant').modal({show: true});
                $('#ecole').modal({show: true});
            </script>
        @endif

    </body>
</html>    