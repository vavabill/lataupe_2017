<nav id="nav" class="navbar navbar-default navbar-inverse navbar-fixed-top">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
        <i class="fa fa-bars"></i>
      </button>
      <a href="/">
          <div class="logo-container">
              <div class="logo">
                  <img src="/images/logo.png" alt="La taupe" title="La taupe - accueil" />
              </div>
          </div>
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="menu" >
        <ul class="nav navbar-nav navbar-right">
            <li> 
                @if( Request::is('ecole'))
                    <a href="/" alt="LA TAUPE - étudiant">APP ÉTUDIANTS</a>
                @else
                    <a href="/ecole" alt="LA TAUPE - profil école">JE RECRUTE</a>
                @endif
            </li>
            <li>
                @if( Request::is('ecole'))
                    <a href="" data-toggle="modal" data-target="#ecole" alt="inscription école">S'INSCRIRE</a>
                @else
                    <a href="" data-toggle="modal" data-target="#etudiant" alt="inscription étudiant">S'INSCRIRE</a>
                @endif
            </li>
            <li>
                <a href="https://www.facebook.com/lataupe.orientation/?fref=ts" title="Facebook La Taupe" target="_blank">NOUS SUIVRE</a>
            </li>
            <li>
                <a href="/pdf/Generation-Z.pdf" title="Génération Z" target="_blank">GÉNÉRATION Z</a>
            </li>
       </ul>
    </div><!-- /.navbar-collapse -->

</nav>