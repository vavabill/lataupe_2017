<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>

        {{-- Foundation Framework style --}}
        @include('emails._foundation-email')

        {{-- Custom styles --}}
        <style type="text/css">
            table.facebook td {
              background: #3b5998;
              border-color: #2d4473;
            }

            table.facebook:hover td {
              background: #2d4473 !important;
            }

            table.twitter td {
              background: #00acee;
              border-color: #0087bb;
            }

            table.twitter:hover td {
              background: #0087bb !important;
            }

            table.google-plus td {
              background-color: #DB4A39;
              border-color: #CC0000;
            }

            table.google-plus:hover td {
              background: #CC0000 !important;
            }
        </style>
    </head>
    <body>
        <table class="body">
            <tr>
                <td class="center" align="center" valign="top">
                    <center>


                        <table class="row header">
                            <tr>
                                <td class="center" align="center">
                                    <center>

                                        <table class="container">
                                            <tr>
                                                <td class="wrapper last">

                                                    <table class="twelve columns">
                                                        <tr>

                                                            <td class="six sub-columns">
                                                                <img src="{{ asset('images/logo.png') }}" alt="LA TAUPE">
                                                            </td>
                                                            <td class="six sub-columns last" align="right" style="text-align:right; vertical-align:middle;">
                                                                <span class="template-label">@yield('title')</span>
                                                            </td>
                                                            <td class="expander"></td>

                                                        </tr>
                                                    </table>

                                                </td>
                                            </tr>
                                        </table>

                                    </center>
                                </td>
                            </tr>
                        </table>
                        <br>

                        <table class="container">
                            <tr>
                                <td>

                                    <!-- content start -->
                                    <table class="row">
                                        <tr>
                                            <td class="wrapper last">

                                                <table class="twelve columns">
                                                    <tr>
                                                        <td>
                                                            @yield('content')
                                                        </td>
                                                        <td class="expander"></td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>


                                    <table class="row footer">
                                        <tr>
                                            <td class="wrapper last">

                                                <table class="six columns">
                                                    <tr>
                                                        <td class="last right-text-pad">

                                                            <h5>Contact</h5>

                                                            <p>
                                                                <strong>LA TAUPE</strong><br />
                                                            </p>

                                                            <a class="btn btn-default" href="{!! url('contact') !!}">Nous contacter</a>
                                                        </td>
                                                        <td class="expander"></td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>

                                    <!-- container end below -->
                                </td>
                            </tr>
                        </table>

                    </center>
                </td>
            </tr>
        </table>
    </body>
</html>
