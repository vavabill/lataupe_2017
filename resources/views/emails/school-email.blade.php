@extends('layouts.email')

@section('title')
Nouvelle inscription LATAUPE
@stop

@section('content')
	Une nouvelle inscription a été reçue via lataupe.fr :

    <ul>
        <li>Nom de l'établissement : {{ $school->establishment }}</li>
		<li>Code postal : {{ $school->cp }}</li>
        <li>Ville : {{ $school->city }}</li>
        <li>Email : {{ $school->email }}</li>
        <li>Téléphone : {{ $school->phone }}</li>
    </ul>

    <br/><hr/><br/>

    <h6>INTERLOCUTEUR :</h6>

    <ul>
    	<li>Fonction : {{ $school->fonction }}</li>
    	<li>Nom : {{ $school->name }}</li>
		<li>Prénom : {{ $school->firstname }}</li>
		<li>Email : {{ $school->contact_email }}</li>
	</ul>
		
        
@stop


