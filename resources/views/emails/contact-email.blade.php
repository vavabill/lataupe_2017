@extends('layouts.email')

@section('title')
Nouveau contact LATAUPE
@stop

@section('content')
	Une nouveau contact a été reçu via lataupe.fr :

    <ul>

        <li>Nom : {{ $contact->name }}</li>

		<li>Prénom : {{ $contact->firstname }}</li>

		@unless (empty($contact->establishment))
	        <li>Établissement : {{ $contact->establishment }}</li>
        @endunless

		<li>Email : {{ $contact->email }}</li>

		@unless (empty($contact->phone))
        	<li>Téléphone : {{ $contact->phone }}</li>
        @endunless


        <br/><hr/><br/>


        <li>Sujet : {{ $contact->subject }}</li>

    </ul>

    <hr>
	<h5>Message :</h5>
	{{ $contact->message }}

@stop
