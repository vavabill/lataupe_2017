@extends('layouts.email')

@section('title')
Nouvelle inscription LATAUPE
@stop

@section('content')
	Une nouvelle inscription a été reçue via lataupe.fr :

    <ul>

        <li>Email : {{ $student->email }}</li>

        <br/><hr/><br/>

        <li>Nom : {{ $student->name }}</li>
		<li>Prénom : {{ $student->firstname }}</li>
		<li>Date de naissance : {{ $student->birthdate }}</li>
        <li>Téléphone : {{ $student->phone }}</li>
        <li>Email : {{ $student->email }}</li>

        <br/><hr/><br/>

        <li>Code postal : {{ $student->cp }}</li>
        <li>Ville : {{ $student->city }}</li>

        <br/><hr/><br/>

        <li>Statut : 
	        @foreach($statut_jeunes as $statut_jeune)
	            @if($statut_jeune->id == $student->statut_id)
	                <?php echo $statut_jeune->name; ?>
	            @endif    
	        @endforeach
        </li>

        @unless (empty($student->class_id))
	        <li>Classe : 
	    		@foreach($classes as $classe)
		            @if($classe->id == $student->class_id)
		                <?php echo $classe->name; ?>
		            @endif    
		        @endforeach
	        </li>
        @endunless

        @unless (empty($student->bac_id))
	        <li>Bac préparé : 
	    		@foreach($bacs as $bac)
		            @if($bac->id == $student->bac_id)
		                <?php echo $bac->name; ?>
		            @endif    
		        @endforeach
	        </li>
        @endunless

        @unless (empty($student->level_id))
	        <li>Niveau : 
	    		@foreach($levels as $level)
		            @if($level->id == $student->level_id)
		                <?php echo $level->name; ?>
		            @endif    
		        @endforeach
	        </li>
        @endunless

        @unless (empty($student->theme_id))
	        <li>Domaine étudié : 
	    		@foreach($themes as $theme)
		            @if($theme->id == $student->theme_id)
		                <?php echo $theme->name; ?>
		            @endif    
		        @endforeach
	        </li>
        @endunless


        <br/><hr/><br/>

        <li>Domaine(s) qui l'intéresse : 
	        @foreach($interests as $interest)
	            @if($interest->student_id == $student->id)
	            	<ul>
		                @foreach($themes as $theme)
				            @if($theme->id == $interest->student_interest_id)
				                <li><?php echo $theme->name; ?></li>
				            @endif    
				        @endforeach
			        </ul>
	            @endif    
	        @endforeach
        </li>
@stop
