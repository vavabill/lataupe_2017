@extends('layouts.app')

@section('title')
    La Taupe - L'Appli pour choisir ses études
@stop

@section('description')
    BTS, Bachelor, Licence, Master…. Trouvez votre future école sur La Taupe, l’appli qui met en relation les jeunes à la recherche de leur orientation post bac et les écoles/universités.
@stop


@section('content')
    <div class="center student">
        @include('layouts.main_nav')

        <div id="myCarousel" class="carousel slide" data-ride="carousel" style="user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); touch-action: none;">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/images/slides/LA-TAUPE--slide-student1.jpg" alt="La taupe : Objectif Bachelor">
                </div>

                <div class="item">
                    <img src="/images/slides/LA-TAUPE--slide-student3.jpg" alt="La taupe : Objectif BTS">
                </div>

                <div class="item">
                    <img src="/images/slides/LA-TAUPE--slide-student2.jpg" alt="La taupe : Objectif Erasmus">
                </div>

                <div class="item">
                    <img src="/images/slides/LA-TAUPE--slide-student4.jpg" alt="La taupe : Objectif Alternance">
                </div>
            </div>
        </div>
        
        <div class="col-md-12 bottom-carousel">
            <div class="col-lg-4 col-lg-offset-2 col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0 app-available">
                <div class="col-md-6 col-xs-6">
                    <img src="/images/android-app.png" alt="android app">
                </div>
                <div class="col-md-6 col-xs-6">
                    <img src="/images/available_app_store.png" alt="app store">
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <h2>L'appli pour choisir ses études</h2>
            </div>
            <div class="clear"></div>
        </div>    
    </div>
    <div class="clear"></div>

    <div id="search-slide" class="col-md-12 center slides">
        <div class="col-md-6 col-xs-12 wow slideInLeft">
            <i class="icon fa fa-search"></i>
            <h2>1. DÉCOUVRE</h2>
            <p>Consulte toutes les possibilités qui s’offrent à toi
            <br/>et like les formations qui te plaisent !</p>
            <a href="" data-toggle="modal" class="btn dark-btn" data-target="#etudiant" alt="inscription étudiant">
                EN SAVOIR +
            </a>
        </div>
        <div class="col-md-6 col-xs-12 wow slideInRight">
            <img src="/images/screen/discover.png" alt="app LA TAUPE : rechercher">
        </div>
    </div>
    <div class="clear dashed-line"></div>

    <div class="col-md-12 center slides middle">
        <div class="col-md-6 col-xs-12 wow slideInLeft">
            <img src="/images/screen/search.png" alt="app LA TAUPE : découvrir">
        </div>
        <div class="col-md-6 col-xs-12 wow slideInRight">
            <i class="icon fa fa-list-ul" aria-hidden="true"></i>
            <h2>2. Recherche</h2>
            <p>Rentre les critères qui te correspondent !
            </p>
            <a href="" data-toggle="modal" class="btn dark-btn" data-target="#etudiant" alt="inscription étudiant">
                JE REJOINS LA COMMUNAUTÉ
            </a>
        </div>
    </div>
    <div class="clear dashed-line"></div>

    <div class="col-md-12 center slides">
        <div class="col-md-6 col-xs-12 wow slideInLeft">
            <i class="icon fa fa-file-text-o"></i>
            <h2>3. POSTULE </h2>
            <p>Chatte avec tes formations coup de cœur
            <br/>et candidate en 1 clic !
            </p>
            <a href="" data-toggle="modal" class="btn dark-btn" data-target="#etudiant" alt="inscription étudiant">
                JE M'INSCRIS
            </a>
        </div>
        <div class="col-md-6 col-xs-12 wow slideInRight">
            <img src="/images/screen/find.png" alt="app LA TAUPE : trouver">
        </div>
    </div>
    <div class="clear"></div>

    <!--<div class="col-md-12 center slides slide-bg student">
        <div class="col-md-6 col-xs-12 wow slideInLeft slide-left">
            <div class="col-md-6 col-xs-6">
                <img src="/images/android-app.png" alt="android app">
            </div>
            <div class="col-md-6 col-xs-6">
                <img src="/images/available_app_store.png" alt="app store">
            </div>
        </div>
        <div class="col-md-6 col-xs-12 wow slideInRight slide-right">
            <img src="/images/screen/coach.png" alt="app LA TAUPE : coach d'orientation">
        </div>
        <div class="clear"></div>
    </div>-->

    <div class="col-md-12 app-sign center wow slideInUp">
        <h2>TA FUTURE ÉCOLE T'ATTEND SUR LA TAUPE</h2>
        <a href="" data-toggle="modal" class="btn btn-primary inscription" data-target="#etudiant" alt="inscription étudiant">
            JE M'INSCRIS
        </a>
        <div class="clear"></div>
        <div class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2 app">
            <div class="col-md-6 col-xs-6">
                <img src="/images/android-app.png" alt="android app">
            </div>
            <div class="col-md-6 col-xs-6">
                <a href="http://appsto.re/fr/5gSPfb.i" target="_blank" title="Disponible sur l'App Store">
                    <img src="/images/available_app_store.png" alt="app store">
                </a>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="col-md-12 center social wow slideInUp">
        @include('partials.social_links')
    </div>  

    <!-- Modal ETUDIANTS -->
    <div class="modal fade" id="etudiant" tabindex="-1" role="dialog" aria-labelledby="ModalEtudiant">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ModalEtudiants">Crée ton profil</h4>
                </div>

                <div class="modal-body">
                    
                    @include('forms._etudiant')
     
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

@endsection
