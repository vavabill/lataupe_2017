@extends('layouts.app')

@section('title')
    La Taupe - Une question, un besoin, contactez-nous
@stop

@section('description')
    BTS, Bachelor, Licence, Master…. Trouvez votre future école sur La Taupe, l’appli qui met en relation les jeunes à la recherche de leur orientation post bac et les écoles/universités.
@stop



@section('content')
    <div class="full center contact">
        <nav id="nav" class="navbar navbar-default navbar-inverse navbar-fixed-top">

		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button id="menu-toggle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
		        <i class="fa fa-bars"></i>
		      </button>
		      <a href="/">
		          <div class="logo-container">
		              <div class="logo">
		                  <img src="/images/logo.png" alt="Hello Extra" title="Hello Extra - accueil" />
		              </div>
		          </div>
		      </a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="menu" >
		        <ul class="nav navbar-nav navbar-right">
		            <li> 
		                <a href="/student" alt="LA TAUPE - étudiant">APP ÉTUDIANTS</a>
		            </li>
		            <li>
		                <a href="/ecole" alt="LA TAUPE - profil école">JE RECRUTE</a>
		            </li>
		       </ul>
		    </div><!-- /.navbar-collapse -->

		</nav>
        
        <div class="head-contact">
            <div class="col-md-6 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 wow slideInLeft slide-left contact-box">
	        	{!! Form::open([
                    'action' => 'ContactsController@store'
                ]) !!}

                    @include('forms._contact')

                    <div class="col-sm-12">
                        {!! Form::submit('Envoyer', ['class' => 'btn btn-primary btn-send', 'name' => 'submit_contact']) !!}
                    </div>

                {!! Form::close() !!}

	        </div>
	        <div class="col-md-4 col-sm-12 col-xs-12 wow slideInRight slide-right">
	            <img src="/images/screen/coach.png" alt="app LA TAUPE : coach d'orientation">
	        </div>
	        <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="col-md-12 center social wow slideInUp">
	    <ul>
			<li>
				<a href="https://www.facebook.com/lataupe.orientation/?fref=ts" alt="Facebook La Taupe" title="Facebook La Taupe" target="_blank">
					<i class="fa fa-facebook"></i>
				</a>
			</li>
			<li>
				<a href="https://www.instagram.com/la_taupe_app/" alt="Instagram La Taupe" title="Instagram La Taupe" target="_blank">
					<i class="fa fa-instagram"></i>
				</a>
			</li>
		</ul>
		<div class="clear"></div>
    </div>

@endsection

