<!-- Modal ETUDIANTS -->
<div class="modal fade" id="etudiant" tabindex="-1" role="dialog" aria-labelledby="ModalEtudiant">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="ModalEtudiants">Crée ton profil</h4>
			</div>

			<div class="modal-body">
				
				@include('forms._etudiant')
 
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>	

<!-- Modal ECOLES -->
<div class="modal fade" id="ecole" tabindex="-1" role="dialog" aria-labelledby="ModalEcole">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="ModalEcole">Inscription écoles</h4>
			</div>

			<div class="modal-body">
				@include('forms.ecole')
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>	


<!-- Modal CONTACT -->
<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="ModalContact">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="ModalContact">Contactez-nous</h4>
			</div>

			<div class="modal-body">
				@include('forms.contact')
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>	