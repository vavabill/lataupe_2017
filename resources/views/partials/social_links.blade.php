<div class="col-md-12 col-xs-12">
    <ul>
    	<li>
			<a href="/contact" alt="contact LA TAUPE" title="Fcontact LA TAUPE">
				<i class="fa fa-envelope"></i>
			</a>
		</li>
		<li>
			<a href="https://www.facebook.com/lataupe.orientation/?fref=ts" alt="Facebook La Taupe" title="Facebook La Taupe" target="_blank">
				<i class="fa fa-facebook"></i>
			</a>
		</li>
		<li>
			<a href="https://www.instagram.com/la_taupe_app/" alt="Instagram La Taupe" title="Instagram La Taupe" target="_blank">
				<i class="fa fa-instagram"></i>
			</a>
		</li>
	</ul>
</div>
<div class="clear"></div>