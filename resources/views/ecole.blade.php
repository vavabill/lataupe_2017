@extends('layouts.app')

@section('title')
    La Taupe - Recrutez vos futurs étudiants !
@stop

@section('description')
    Bénéficier de candidatures qualifiées tout au long de l’année. La Taupe met en relation les jeunes à la recherche de leur orientation post bac et les écoles/universités.
@stop


@section('content')
    <div class="full center school">
        @include('layouts.main_nav')
        
        <div class="head">
            <h1>Recrutez vos futurs étudiants.</h1>
            <h2>La Taupe, l’appli qui joue les entremetteurs entres jeunes et écoles.</h2>
        </div>
        <div class="col-md-6 col-md-offset-3 app-available">
            <a href="" data-toggle="modal" class="btn dark-btn" data-target="#ecole" alt="inscription école">
                S'INSCRIRE
            </a>
        </div>
        <!--<div class="col-md-6 col-md-offset-3 app-available">
            <div class="col-md-6 col-xs-6">
                <img src="/images/android-app.png" alt="android app">
            </div>
            <div class="col-md-6 col-xs-6">
                <img src="/images/available_app_store.png" alt="app store">
            </div>
        </div>-->
    </div>
    <div class="clear"></div>

    <div class="col-md-12 center slides middle">
        <div class="col-md-6 wow slideInLeft">
            <img src="/images/screen/ecole.png" alt="app LA TAUPE" />
        </div>
        <div class="col-md-6 wow slideInRight">
            <i class="icon fa fa-line-chart" aria-hidden="true"></i>
            <h2 class="title-school">RECEVEZ LES CANDIDATURES
            <br/>D'ÉTUDIANTS INTÉRESSÉS
            <br/>PAR VOS FORMATIONS !</h2>
            <a href="" data-toggle="modal" class="btn dark-btn" data-target="#ecole" alt="inscription école">
                EN SAVOIR +
            </a>
        </div>
    </div>
    <div class="clear dashed-line"></div>

    <div class="col-md-12 center slides middle wow slideInUp">
        <h2 class="title dark">Comment ça marche ?</h2>
        <div class="col-md-4">
            <h3>1. Communiquez</h3>
            <p class="how">Publiez votre offre pédagogique et vos actualités en live.</p>
        </div>
        <div class="col-md-4">
            <h3>2. Attirez</h3>
            <p class="how">Identifiez votre audience et ciblez les profils adaptés à vos formations.</p>
        </div>
        <div class="col-md-4">
            <h3>3. Recrutez</h3>
            <p class="how">Bénéficiez de candidatures qualifiées tout au long de l’année.</p>
        </div>
        <div class="col-md-12 center">
            <a href="" data-toggle="modal" class="btn dark-btn school-btn" data-target="#ecole" alt="inscription école">
                S'INSCRIRE
            </a>
        </div>
    </div>
    
    <div class="clear dashed-line"></div>
    <div class="col-md-8 col-md-offset-2 center slides function wow slideInUp">
        <h2 class="title dark">Vos fonctionnalités</h2>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="picto audience">
                <p>Référencement pertinent</p>
            </div>    
            <div class="picto campagne">
                <p>Campagne de publicité ciblée</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="picto kit">
                <p>Kit de communication personnalisé</p>
            </div>
            <div class="picto chat">
                <p>Chat direct étudiants</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="picto search">
                <p>Statistiques et analyse de l’audience</p>
            </div>
            <div class="picto register">
                <p>Système de candidature en ligne</p>
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <!--<div class="col-md-12 center slides slide-bg">
        <div class="col-md-6 wow slideInLeft slide-left"></div>
        <div class="col-md-6 wow slideInRight slide-right">
            <img src="/images/screen/coach.png" alt="app LA TAUPE : coach d'orientation">
        </div>
        <div class="clear"></div>
    </div>-->

    <!--
    <div class="col-md-12 app-sign center wow slideInUp">
        <a href="" data-toggle="modal" class="btn dark-btn btn-little" data-target="#ecole" alt="inscription école">
            VOS FUTURS ÉTUDIANTS VOUS ATTENDENT SUR LA TAUPE
        </a>
        <div class="clear"></div>
    </div>-->

    <div class="col-md-12 app-sign center wow slideInUp">
        <h2>VOS FUTURS ÉTUDIANTS VOUS ATTENDENT SUR LA TAUPE</h2>
        <a href="" data-toggle="modal" class="btn btn-primary inscription" data-target="#ecole" alt="inscription école">
            S'INSCRIRE
        </a>
        <div class="clear"></div>
        <div class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2 app">
            <div class="col-md-6 col-xs-6">
                <img src="/images/android-app.png" alt="android app">
            </div>
            <div class="col-md-6 col-xs-6">
                <a href="http://appsto.re/fr/5gSPfb.i" target="_blank" title="Disponible sur l'App Store">
                    <img src="/images/available_app_store.png" alt="app store">
                </a>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="col-md-12 center social wow slideInUp">
        @include('partials.social_links')
    </div>  

    <!-- Modal ECOLES -->
    <div class="modal fade" id="ecole" tabindex="-1" role="dialog" aria-labelledby="ModalEcole">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="ModalEcole">Inscription écoles</h4>
                </div>

                <div class="modal-body">
                    @include('forms._ecole')
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

@endsection

