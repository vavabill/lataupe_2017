$(document).ready(function(){
    $("select.statut").change(function(){
        $(this).find("option:selected").each(function(){
            if($(this).attr("value")=="1"){
                $(".box").not(".collegien").hide();
                $(".collegien").show();
            }
            else if($(this).attr("value")=="2"){
                $(".box").not(".lyceen").hide();
                $(".lyceen").show();
            }
            else if($(this).attr("value")=="3"){
                $(".box").not(".etudiant").hide();
                $(".etudiant").show();
            }
            else{
                $(".box").hide();
            }
        });
    }).change();
});

$('#interest_list').select2();

var wow = new WOW(
  {
    boxClass:     'wow',
    animateClass: 'animated',
    offset:       0,
    mobile:       true,
    live:         true,
    callback:     function(box) {
      // 
    },
    scrollContainer: null
  }
);
wow.init();

// SWIPE MOBILE
$(document).ready(function() {  
        $('#myCarousel').hammer().on('swipeleft', function(){
            $(this).carousel('next'); 
        })
        $('#myCarousel').hammer().on('swiperight', function(){
            $(this).carousel('prev'); 
        })
 }); 