<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/




Route::resource('/', 'StudentsController');

Route::resource('/student', 'StudentsController');
Route::get('/confirmations/student', 'StudentsController@confirmation');

Route::resource('/ecole', 'SchoolsController');
Route::get('/confirmations/ecole', 'SchoolsController@confirmation');

Route::resource('/contact', 'ContactsController');


// SITEMAP
Route::get('sitemap', function(){

    // create new sitemap object
    $sitemap = App::make("sitemap");

    // set cache key (string), duration in minutes (Carbon|Datetime|int), turn on/off (boolean)
    // by default cache is disabled
    $sitemap->setCache('laravel.sitemap', 60);

    // check if there is cached sitemap and build new only if is not
    if (!$sitemap->isCached())
    {
        // add item to the sitemap (url, date, priority, freq)
        $sitemap->add(URL::to('/'), Carbon\Carbon::now(), '1.0', 'daily');

        // add every elements to the sitemap
        $sitemap->add(URL::to('/student'), Carbon\Carbon::now(), '1.0', 'weekly');

        $sitemap->add(URL::to('/ecole'), Carbon\Carbon::now(), '1.0', 'weekly');

        $sitemap->add(URL::to('/contact'), Carbon\Carbon::now(), '1.0', 'monthly');
    }

    // show your sitemap (options: 'xml' (default), 'html', 'txt', 'ror-rss', 'ror-rdf')
    return $sitemap->render('xml');

});
