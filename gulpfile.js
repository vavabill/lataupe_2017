const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'public'        : 'public/',
    'assets'        : 'resources/assets/',
    'font_awesome'  : 'node_modules/font-awesome/',
    'bootstrap'		: 'node_modules/bootstrap-sass/',
    'jquery'		: 'node_modules/jquery/',
    'select2'       : 'node_modules/select2/',
    'wow'           : 'node_modules/wowjs/'
}


elixir(mix => {
    mix.sass('app.scss')
       .webpack('app.js');

    // Scripts
    mix.scripts(
        [
            paths.jquery        + 'dist/jquery.js',
            paths.bootstrap     + 'assets/javascripts/bootstrap.js',
            paths.select2       + 'dist/js/select2.js',
            paths.wow           + 'dist/wow.min.js',
            paths.assets        + 'js/hammer.min.js',
            paths.assets        + 'js/front.js',
        ], 'public/js/front.js', './');


    // FONTS
    mix.copy(paths.font_awesome + 'fonts', paths.public + 'fonts');

	// IMAGES
    mix.copy(paths.assets + 'images', paths.public + 'images');

    // PDF
    mix.copy(paths.assets + 'pdf', paths.public + 'pdf');

});    
