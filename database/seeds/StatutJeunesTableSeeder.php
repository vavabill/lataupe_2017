<?php

use Illuminate\Database\Seeder;

class StatutJeunesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuts = array(
            'Collégien(ne)',
            'Lycéen(ne)',
            'Étudiant(e)',
        );

        foreach ($statuts as $statut) {
            DB::table('statut_jeunes')->insert(array('name' => $statut));
        }
    }
}
