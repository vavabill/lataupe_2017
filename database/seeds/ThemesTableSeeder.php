<?php

use Illuminate\Database\Seeder;

class ThemesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('themes')->delete();

        $themes = [
            'Esthétique',
            'Mode',
            'Tourisme',
            'Sport',
            'Commerce',
            'Communication',
            'Informatique',
            'Paramédical',
            'Social',
            'Transport',
            'Architecture d’intérieur',
            'Art',
            'Droit',
            'Audiovisuel',
        ];

        foreach($themes as $theme) {
            DB::table('themes')->insert(array('name' => $theme));
            // Theme::create([
            //     'name'        => $theme,
            //     'description' => '',
            //     'short_intro' => '',
            //     'logo'        => '',
            //     'url'         => '',
            // ]);
        }
    }

}
