<?php

use Illuminate\Database\Seeder;

class BacTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuts = array(
            'S',
            'ES',
            'L',
            'STMG',
            'STI2D',
            'STD2A',
            'STL',
            'ST2S',
            'STAV',
            'TMD',
            'Professionnel',
            'Technologique hôtellerie (STHR)'
        );

        foreach ($statuts as $statut) {
            DB::table('bacs')->insert(array('name' => $statut));
        }
    }
}
