<?php

use Illuminate\Database\Seeder;

class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuts = array(
            'CAP',
            'Troisième',
            'Seconde',
            'Seconde pro',
            'Première',
            'Première pro',
            'Terminale',
            'Terminale pro'
        );

        foreach ($statuts as $statut) {
            DB::table('class')->insert(array('name' => $statut));
        }
    }
}
