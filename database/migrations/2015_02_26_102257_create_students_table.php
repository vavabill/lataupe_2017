<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('firstname');
			$table->text('testimony', 65535);
			$table->text('testimony_short', 65535);
			$table->text('school_career', 65535);
			$table->text('higher_education', 65535);
			$table->text('career', 65535);
			$table->text('experience', 65535);
			$table->text('job_definition', 65535);
			$table->text('carrer_evolution', 65535)->nullable();
			$table->date('birthdate');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('students');
	}

}
