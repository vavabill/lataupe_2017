<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropForeignKeysToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function ($table) {
            $table->dropForeign('students_statut_id_foreign');
            $table->dropForeign('students_class_id_foreign');
            $table->dropForeign('students_bac_id_foreign');
            $table->dropForeign('students_level_id_foreign');
            $table->dropForeign('students_theme_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
