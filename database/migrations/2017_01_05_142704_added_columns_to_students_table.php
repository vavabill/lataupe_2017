<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedColumnsToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function(Blueprint $table)
        {
            $table->integer('statut_id')->unsigned()->nullable();
                $table->foreign('statut_id')->references('id')->on('statut_jeunes');

            $table->integer('class_id')->unsigned()->nullable();
                $table->foreign('class_id')->references('id')->on('class');

            $table->integer('bac_id')->unsigned()->nullable();
                $table->foreign('bac_id')->references('id')->on('bacs');

            $table->integer('level_id')->unsigned()->nullable();
                $table->foreign('level_id')->references('id')->on('levels');

            $table->integer('theme_id')->unsigned()->nullable();
                $table->foreign('theme_id')->references('id')->on('themes');

            $table->integer('civility')->nullable();
            $table->string('phone', 15)->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('cp', 10)->nullable();
            $table->string('city')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function(Blueprint $table)
        {
            $table->dropColumn('statut_id');
            $table->dropColumn('class_id');
            $table->dropColumn('bac_id');
            $table->dropColumn('level_id');
            $table->dropColumn('theme_id');
            $table->dropColumn('civility');
            $table->dropColumn('phone');
            $table->dropColumn('email');
            $table->dropColumn('address');
            $table->dropColumn('cp');
            $table->dropColumn('city');
        });
    }
}

